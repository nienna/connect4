# Haskell Connect Four
A Haskell Connect Four game
## How to play
Compile the program in Haskell using the following order
```bash
ghc -game.hs
```
If the Linux distribution does not support Haskell's static libraries, as is the case with this version, or if compiling with the static library causes problems, use it:
``bash
ghc -dynamic game.hs
```
With the compiled program run with
``bash
./game.hs
```

Once executed the program will request the size of the board and expects to receive two numbers larger than 0 separated by a space, the first number is the height and the second the width. If no number is given, a default 6x7 tile is generated

Then you have to indicate the game mode, where you choose one of the three artificial inteligences of your opponents or if you want to duel with two humans

The game will randomly determine the initial player and will ask for the column where you want to drop the tile by showing the tile on the screen

## Artificial intelligence

The game includes a set of 3 different Inteligences

### Random

The AI Random randomly decides the column where the card will be placed

### Greddy

Greddy evaluates the free positions on the board and scores the columns according to who brings him closer to victory each column, the victory that month approaches the victory will be the one chosen, ignoring the rest of factors

### Smart

Smart tea a similar behavior to Greddy, it will score the whole board, but in this case the score is not only evaluating its tiles to the tile, also those of the opponent and if it is the case, making moves that prevent the player from making a 4 in a row

