import System.Random
import System.IO
import Debug.Trace
import System.Exit
import Data.List


inTauler :: [Int] -> [Int] -> Bool
inTauler pos size
    | (head pos) < 0 = False
    | (last pos) < 0 = False
    | (head pos) >= (head size) = False
    | (last pos) >= (last size) = False
    | otherwise = True

evaluarTaulell :: [[String]] -> [Int] -> [Int] -> [String] -> Int -> Bool
evaluarTaulell tauler pos size condicio longitut = checkx || checky || checxy || checkyx
    where
        fila = tauler !! ((head size) - (head pos))
        casella = fila !! (last pos)
        checkx = comprobarx tauler [(head pos)-longitut,(last pos)] size casella [] condicio longitut
        checky = comprobary tauler [(head pos),(last pos)-longitut] size casella [] condicio longitut
        checxy = comprobarxy tauler [(head pos)-longitut,(last pos)-longitut] size casella [] condicio longitut
        checkyx = comprobaryx tauler [(head pos)+longitut,(last pos)+longitut] size casella [] condicio longitut

comprobarx:: [[String]] -> [Int] -> [Int] -> String -> [String] -> [String] -> Int -> Bool
comprobarx tauler pos size ficha linea condicio longitut
    | length linea == (longitut*2+1) = isInfixOf condicio linea
    | not (inTauler pos size) = comprobarx tauler [(head pos)+1,(last pos)] size ficha (linea ++ ["-"]) condicio longitut
    | otherwise = comprobarx tauler [(head pos)+1,(last pos)] size ficha (linea ++ [casella]) condicio longitut
         where 
            casella = fila !! (last pos)
            fila = tauler !! ((head size) - (head pos)-1)

comprobary:: [[String]] -> [Int] -> [Int] -> String -> [String] -> [String] -> Int -> Bool
comprobary tauler pos size ficha linea condicio longitut
    | length linea == (longitut*2+1) = isInfixOf condicio linea
    | not (inTauler pos size) = comprobary tauler [(head pos),(last pos)+1] size ficha (linea ++ ["-"]) condicio longitut
    | otherwise = comprobary tauler [(head pos),(last pos)+1] size ficha (linea ++ [casella]) condicio longitut
         where 
            casella = fila !! (last pos)
            fila = tauler !! ((head size) - (head pos)-1)

comprobarxy:: [[String]] -> [Int] -> [Int] -> String -> [String] -> [String] -> Int -> Bool
comprobarxy tauler pos size ficha linea condicio longitut
    | length linea == (longitut*2+1) = isInfixOf condicio linea
    | not (inTauler pos size) = comprobarxy tauler [(head pos)+1,(last pos)+1] size ficha (linea ++ ["-"]) condicio longitut
    | otherwise = comprobarxy tauler [(head pos)+1,(last pos)+1] size ficha (linea ++ [casella]) condicio longitut
         where 
            casella = fila !! (last pos)
            fila = tauler !! ((head size) - (head pos)-1)

comprobaryx:: [[String]] -> [Int] -> [Int] -> String -> [String] -> [String] -> Int -> Bool
comprobaryx tauler pos size ficha linea condicio longitut
    | length linea == (longitut*2+1) = isInfixOf condicio linea
    | not (inTauler pos size) = comprobaryx tauler [(head pos)-1,(last pos)-1] size ficha (linea ++ ["-"]) condicio longitut
    | otherwise = comprobaryx tauler [(head pos)-1,(last pos)-1] size ficha (linea ++ [casella]) condicio longitut
         where 
            casella = fila !! (last pos)
            fila = tauler !! ((head size) - (head pos)-1)
    
setStatus :: [[String]] -> String -> [[String]]
setStatus tauler status =trace ("Newtauler: " ++ show newtauler) newtauler
    where
        newtauler = trace ("NewFila2: "++ show newfila) ([newfila] ++ antictauler)
        newfila = trace ("NewFila: "++ show (init (head (fst migtauler)))) (init (head(fst migtauler)) ++ [status])
        antictauler = trace ("OldaTauler: " ++ show (snd migtauler))(snd migtauler)
        migtauler = splitAt 1 tauler