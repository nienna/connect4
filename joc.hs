import System.Random
import System.IO
import System.Exit
import Data.List
import Data.Maybe

-- Funcio oer convertir un string en un enter
atoi :: String -> Int
atoi s = read s :: Int

randInt :: Int -> Int -> IO Int
-- randInt low high is an IO action that returns a
-- pseudo-random integer between low and high (both included).
randInt low high = do
    random <- randomIO :: IO Int
    let result = low + random `mod` (high - low + 1)
    return result
-- ############################################### --
-- Funcions relacionades amb la gestio del taulell --
-- ############################################### --

-- Funcio que donada una posicio del taulell cambia la casella de buit per la fitxa corresponent al torn
afegirFitxa :: [[String]] -> Int -> [Int] -> [Int] -> [[[String]]]
afegirFitxa tauler torn size [0,x] = do
    let fila = last tauler
    let casella = fila !! x
    if casella == ['.'] then 
        return (head (replaceficha tauler torn size [1,x]))
    else
        return (head (replaceficha tauler torn size [2,x]))

afegirFitxa tauler torn size pos = do
    let fila = tauler !! ((head size) - (head pos))
    let casella = fila !! (last pos)
    if casella == ['.'] then
        afegirFitxa tauler torn size [(head pos)-1,(last pos)]
    else do
        show 2
        return (head(replaceficha tauler torn size [(head pos)+1,(last pos)]))

-- funcio que canvia el estat del joc al indicat en el segon camp de la funcio
changeStatus :: [[String]] -> String -> [[String]]
changeStatus tauler status = newtauler
    where
        newtauler = [newfila] ++ antictauler
        newfila = init (head(fst migtauler)) ++ [status]
        antictauler = snd migtauler
        migtauler = splitAt 1 tauler

--Funcio que crea una de les files del taulell amb les caselles buides
crearLlista :: Int -> [String]
crearLlista 0 = []
crearLlista n = crearLlista (n-1) ++ ["."]

--Funcio que donades unes mides en x e y construix el taulell de joc
crearTaulell :: [Int] -> [[String]]
crearTaulell [] = crearTaulell[6,7]
crearTaulell [1,y] = [crearLlista(y+1)]
crearTaulell [x,y] = crearTaulell[x-1,y] ++ [crearLlista(y)]

--Funcio de final de la partida, on indica el jugador i finalitza correctament la execucio del programa
endGame :: [[String]] -> Int -> IO a
endGame tauler torn = do
    if torn == 0 then
        putStrLn "Guanya l'huma"
    else
        putStrLn "Guanya la maquina"
    imprimirTaulell tauler
    exitSuccess

--Funcio que comproba si la posicio indicada es troba dins de les mides del taulell
inTauler :: [Int] -> [Int] -> Bool
inTauler pos size
    | (head pos) < 0 = False
    | (last pos) < 0 = False
    | (head pos) >= (head size) = False
    | (last pos) >= (last size) = False
    | otherwise = True

--Funcio que imprimeix el taulell de joc per pantalla
imprimirTaulell :: [[String]] -> IO()
imprimirTaulell [] = do
    putStr ""
imprimirTaulell (x:xs) = do
    imprimirLinea(x)
    imprimirTaulell(xs)

--Funcio que imprimeix per pantalla una de les files del taulell
imprimirLinea :: [String] -> IO ()
imprimirLinea [] = do
    putStrLn ""
imprimirLinea (x:xs) = do
    putStr x
    imprimirLinea xs
--Funcio que remplaça un caracter en una fila a la posicio n per el caracter indicat a ficha
replaceafila :: Int -> [Char] -> [String] -> [String]
replaceafila n ficha (x:xs)
    | n == 0 = ficha:xs
    | otherwise = x:replaceafila (n-1) ficha xs

--Funcio que donada una posicio en el taulell remplaça una fitxa per una altra
replaceficha :: [[String]] -> Int -> [Int] -> [Int] -> [[[String]]]
replaceficha tauler torn size pos = do
    let x = last pos
    let y = (head size) - (head pos) + 1
    if torn == 0 then do
        let migtauler = splitAt (y-1) tauler
        let fila = replaceafila x ['X'] (head (snd migtauler))
        let newtauler = fst migtauler ++ [fila] ++ tail (snd migtauler)
        if evaluarTaulell newtauler [(head pos)-1, last pos] size ["X","X","X","X"] 4 then
            return (changeStatus newtauler "W")
        else
            return (changeStatus newtauler "J")
    else do
        let migtauler = splitAt (y-1) tauler
        let fila = replaceafila x ['O'] (head (snd migtauler))
        let newtauler = fst migtauler ++ [fila] ++ tail (snd migtauler)
        if evaluarTaulell newtauler [(head pos)-1, last pos] size ["O","O","O","O"] 4 then 
            return (changeStatus newtauler "W")
        else
            return (changeStatus newtauler "J")

-- ################################################ --
--Funcions que busquen conjunts de fitxes al taulell--
-- ################################################ --

-- Funcio que donada una condicio de fitxes en una longitud, comproba si es troben en horitzontal
comprobarx:: [[String]] -> [Int] -> [Int] -> String -> [String] -> [String] -> Int -> Bool
comprobarx tauler pos size ficha linea condicio longitut
    | length linea == (longitut*2+1) = isInfixOf condicio linea
    | not (inTauler pos size) = comprobarx tauler [(head pos)+1,(last pos)] size ficha (linea ++ ["-"]) condicio longitut
    | otherwise = comprobarx tauler [(head pos)+1,(last pos)] size ficha (linea ++ [casella]) condicio longitut
         where 
            casella = fila !! (last pos)
            fila = tauler !! ((head size) - (head pos)-1)

-- Funcio que donada una condicio de fitxes en una longitud, comproba si es troben en vertical
comprobary:: [[String]] -> [Int] -> [Int] -> String -> [String] -> [String] -> Int -> Bool
comprobary tauler pos size ficha linea condicio longitut
    | length linea == (longitut*2+1) = isInfixOf condicio linea
    | not (inTauler pos size) = comprobary tauler [(head pos),(last pos)+1] size ficha (linea ++ ["-"]) condicio longitut
    | otherwise = comprobary tauler [(head pos),(last pos)+1] size ficha (linea ++ [casella]) condicio longitut
         where 
            casella = fila !! (last pos)
            fila = tauler !! ((head size) - (head pos)-1)

-- Funcio que donada una condicio de fitxes en una longitud, comproba si es troben en diagonal, de abaix a adalt 
comprobarxy:: [[String]] -> [Int] -> [Int] -> String -> [String] -> [String] -> Int -> Bool
comprobarxy tauler pos size ficha linea condicio longitut
    | length linea == (longitut*2+1) = isInfixOf condicio linea
    | not (inTauler pos size) = comprobarxy tauler [(head pos)+1,(last pos)+1] size ficha (linea ++ ["-"]) condicio longitut
    | otherwise = comprobarxy tauler [(head pos)+1,(last pos)+1] size ficha (linea ++ [casella]) condicio longitut
         where 
            casella = fila !! (last pos)
            fila = tauler !! ((head size) - (head pos)-1)

-- Funcio que donada una condicio de fitxes en una longitud, comproba si es troben en diagonal, de adalt a abaix 
comprobaryx:: [[String]] -> [Int] -> [Int] -> String -> [String] -> [String] -> Int -> Bool
comprobaryx tauler pos size ficha linea condicio longitut
    | length linea == (longitut*2+1) = isInfixOf condicio linea
    | not (inTauler pos size) = comprobaryx tauler [(head pos)-1,(last pos)-1] size ficha (linea ++ ["-"]) condicio longitut
    | otherwise = comprobaryx tauler [(head pos)-1,(last pos)-1] size ficha (linea ++ [casella]) condicio longitut
         where 
            casella = fila !! (last pos)
            fila = tauler !! ((head size) - (head pos)-1)

--Funcio que donada una posicio, amb una condicio de fitxes i una longitud, comproba si es troba el conjunt de fitxes en les 8 direccions posibles 
evaluarTaulell :: [[String]] -> [Int] -> [Int] -> [String] -> Int -> Bool
evaluarTaulell tauler pos size condicio longitut = checkx || checky || checxy || checkyx
    where
        fila = tauler !! ((head size) - (head pos))
        casella = fila !! (last pos)
        checkx = comprobarx tauler [(head pos)-longitut,(last pos)] size casella [] condicio longitut
        checky = comprobary tauler [(head pos),(last pos)-longitut] size casella [] condicio longitut
        checxy = comprobarxy tauler [(head pos)-longitut,(last pos)-longitut] size casella [] condicio longitut
        checkyx = comprobaryx tauler [(head pos)+longitut,(last pos)+longitut] size casella [] condicio longitut
    

-- ######################### --
-- Funcions per la IA Random --
-- ######################### --

-- Mode de joc de la IA Random, on s'alternan les accions del jugador i la IA i s'afegeixen les fitxes
-- Tauler / jugador (0 es Huma) / Columnes Tauler /Mode de IA / fi partida
jugarRandom :: [[String]] -> Int -> [Int] -> IO ()
jugarRandom tauler 0 size = do
    putStrLn "Juga l'Huma"
    --print tauler
    imprimirTaulell tauler
    imprimirLinea(map show (map (flip mod 10) [1..last size]))
    putStrLn "Tria Columna"
    hFlush stdout
    aux <- getLine
    print aux
    let columna = map atoi (words aux)
    let newtauler = head (afegirFitxa tauler 0 size [(head size)-1,((head columna)-1)])
    if last (head newtauler) == ['W'] then
        endGame newtauler 0
    else
        jugarRandom newtauler 1 size

jugarRandom tauler 1 size = do
    putStrLn "Juga la Maquina"
    --imprimirTaulell tauler
    columna <- randInt 1 (last size)
    print columna
    let newtauler = head (afegirFitxa tauler 1 size [(head size)-1,(columna-1)])
    jugarRandom newtauler 0 size
    if last (head newtauler) == ['W'] then
        endGame newtauler 0
    else
        jugarRandom newtauler 0 size

-- ######################### --
-- Funcions per la IA Greddy --
-- ######################### --

-- Funcio que calcula les puntuacion del taulell per Greddy
puntuarTaulell :: [[String]] -> [Int] -> [Int]-> Int -> [Int]
puntuarTaulell tauler size puntuacions posicio
    | posicio >= (last size) = puntuacions
    | otherwise = puntuarTaulell tauler size (puntuacions ++ [puntuarColumna tauler size posicio 0 0]) (posicio+1)

-- Funcio que calcula la puntuacio total de cada columna segons Greddy
puntuarColumna :: [[String]] -> [Int] -> Int -> Int -> Int -> Int 
puntuarColumna tauler size poscolumna poscasela punts
    | poscasela >= (head size) || casella /= "." = punts
    | otherwise = puntuarColumna tauler size poscolumna (poscasela+1) (punts+(evaluarCasella tauler size poscolumna poscasela))
        where
            casella = fila !! poscolumna
            fila = tauler !! poscasela

-- Funcio que calcula la puntuacio de cada casella segons Greddy
evaluarCasella :: [[String]] -> [Int] -> Int -> Int -> Int
evaluarCasella tauler size poscolumna poscasela = colocable + inrangA + inrangB + inrangC
    where
        colocable
            | (head size) - poscasela <= 4 = 1
            | otherwise = -5
        inrangA
            | evaluarTaulell tauler [((head size) - (poscasela+1)),poscolumna] size ["O"] 1 = 1
            | otherwise = 0
        inrangB
            | evaluarTaulell tauler [((head size) - (poscasela+1)),poscolumna] size ["O","O"] 2 = 2
            | otherwise = 0
        inrangC
            | evaluarTaulell tauler [((head size) - (poscasela+1)),poscolumna] size["O","O","O"] 3 = 3
            | otherwise = 0

-- Funcio de Greddy per calcular la millor columna on colocar fitxa
calculsGreddy :: [[String]] -> [Int] -> Int
calculsGreddy tauler size = fromMaybe (-1) columna
    where
        columna = findIndex (==maxim) puntuacio
        maxim = maximum puntuacio
        puntuacio = puntuarTaulell tauler size [] 0


-- Mode de joc de la IA Greddy, on s'alternan les accions del jugador i la IA i s'afegeixen les fitxes
jugarGreddy :: [[String]] -> Int -> [Int] -> IO ()
jugarGreddy tauler 0 size = do
    putStrLn "Juga l'Huma"
    --print tauler
    imprimirTaulell tauler
    imprimirLinea(map show (map (flip mod 10) [1..last size]))
    putStrLn "Tria Columna"
    hFlush stdout
    aux <- getLine
    let columna = map atoi (words aux)
    let newtauler = head (afegirFitxa tauler 0 size [(head size)-1,((head columna)-1)])
    if last (head newtauler) == ['W'] then
        endGame newtauler 0
    else
        jugarGreddy newtauler 1 size


jugarGreddy tauler 1 size = do
    putStrLn "Juga la Maquina"
    --imprimirTaulell tauler
    let columna = calculsGreddy tauler size
    print columna
    let newtauler = head (afegirFitxa tauler 1 size [(head size)-1,(columna)])
    if last (head newtauler) == ['W'] then
        endGame newtauler 1
    else
        jugarGreddy newtauler 0 size

-- ######################## --
-- Funcions per la IA Smart --
-- ######################## --

-- Funcio que calcula les puntuacion del taulell per Smart
puntuarTaulellSmart :: [[String]] -> [Int] -> [Int]-> Int -> [Int]
puntuarTaulellSmart tauler size puntuacions posicio
    | posicio >= (last size) = puntuacions
    | otherwise = puntuarTaulellSmart tauler size (puntuacions ++ [puntuarColumnaSmart tauler size posicio 0 0]) (posicio+1)

-- Funcio que calcula la puntuacio total de cada columna segons Greddy
puntuarColumnaSmart :: [[String]] -> [Int] -> Int -> Int -> Int -> Int 
puntuarColumnaSmart tauler size poscolumna poscasela punts
    | poscasela >= (head size) || casella /= "." = punts
    | otherwise = puntuarColumnaSmart tauler size poscolumna (poscasela+1) (punts+(evaluarCasellaSmart tauler size poscolumna poscasela))
        where
            casella = fila !! poscolumna
            fila = tauler !! poscasela
        
-- Funcio que calcula la puntuacio de cada casella segons Smart
evaluarCasellaSmart :: [[String]] -> [Int] -> Int -> Int -> Int
evaluarCasellaSmart tauler size poscolumna poscasela = colocable + inrangA + inrangB + inrangC + counterhuma
    where
        colocable
            | (head size) - poscasela <= 4 = 1
            | otherwise = -5
        inrangA
            | evaluarTaulell tauler [((head size) - (poscasela+1)),poscolumna] size ["O"] 1 = 1
            | otherwise = 0
        inrangB
            | evaluarTaulell tauler [((head size) - (poscasela+1)),poscolumna] size ["O","O"] 2 = 2
            | otherwise = 0
        inrangC
            | evaluarTaulell tauler [((head size) - (poscasela+1)),poscolumna] size ["O","O","O"] 3 = 3
            | otherwise = 0
        counterhuma
            | evaluarTaulell tauler [((head size) - (poscasela+1)),poscolumna] size ["X","X","X"] 3 = 60
            | evaluarTaulell tauler [((head size) - (poscasela+1)),poscolumna] size ["X","X"] 2 = 30
            | evaluarTaulell tauler [((head size) - (poscasela+1)),poscolumna] size ["X"] 1 = 15
            | otherwise = 0

-- Funcio de Smart per calcular la millor columna on colocar fitxa     
calculsSmart :: [[String]] -> [Int] -> Int
calculsSmart tauler size = fromMaybe (-1) columna
    where
        columna = findIndex (==maxim) puntuacio
        maxim = maximum puntuacio
        puntuacio = puntuarTaulellSmart tauler size [] 0


-- Mode de joc de la IA Smart, on s'alternan les accions del jugador i la IA i s'afegeixen les fitxes
jugarSmart :: [[String]] -> Int -> [Int] -> IO ()
jugarSmart tauler 0 size = do
    putStrLn "Juga l'Huma"
    --print tauler
    imprimirTaulell tauler
    imprimirLinea(map show (map (flip mod 10) [1..last size]))
    putStrLn "Tria Columna"
    hFlush stdout
    aux <- getLine
    let columna = map atoi (words aux)
    let newtauler = head (afegirFitxa tauler 0 size [(head size)-1,((head columna)-1)])
    if last (head newtauler) == ['W'] then
        endGame newtauler 0
    else
        jugarSmart newtauler 1 size -- No maquina


jugarSmart tauler 1 size = do
    putStrLn "Juga la Maquina"
    --imprimirTaulell tauler
    let columna = calculsSmart tauler size
    print columna
    let newtauler = head (afegirFitxa tauler 1 size [(head size)-1,(columna)])
    if last (head newtauler) == ['W'] then
        endGame newtauler 1
    else
        jugarSmart newtauler 0 size -- No maquina

-- #################################### --
-- Funcions per el mode de dos jugadors --
-- #################################### --

-- Mode de joc del duel de humans, on s'alternan les accions dels jugadors s'afegeixen les fitxes
jugarDuel :: [[String]] -> Int -> [Int] -> IO ()
jugarDuel tauler 0 size = do
    putStrLn "Juga l'Huma A"
    --print tauler
    imprimirTaulell tauler
    imprimirLinea(map show (map (flip mod 10) [1..last size]))
    putStrLn "Tria Columna"
    hFlush stdout
    aux <- getLine
    let columna = map atoi (words aux)
    let newtauler = head (afegirFitxa tauler 0 size [(head size)-1,((head columna)-1)])
    if last (head newtauler) == ['W'] then
        endGame newtauler 0
    else
        jugarDuel newtauler 1 size -- No maquina


jugarDuel tauler 1 size = do
    putStrLn "Juga l'Huma B"
    --print tauler
    imprimirTaulell tauler
    imprimirLinea(map show (map (flip mod 10) [1..last size]))
    putStrLn "Tria Columna"
    hFlush stdout
    aux <- getLine
    let columna = map atoi (words aux)
    let newtauler = head (afegirFitxa tauler 1 size [(head size)-1,((head columna)-1)])
    if last (head newtauler) == ['W'] then
        endGame newtauler 1
    else
        jugarDuel newtauler 0 size -- No maquina

-- ############################ --
-- Funcions per l'inici del joc --
-- ############################ --

-- Funcio on es determina el mode de joc desitxat
modeJoc :: [[String]] -> [Int] -> IO ()
modeJoc x size = do 
    putStrLn "Tria el mode de joc"
    putStrLn "1) Random"
    putStrLn "2) Greddy"
    putStrLn "3) Smart"
    putStrLn "4) Huma vs Huma"
    aux <- getLine
    let mode = head (words aux)
    juginici <- randInt 0 1
    if mode == ['1'] then
        jugarRandom x juginici size
    else if mode == ['2'] then
        jugarGreddy x juginici size
    else if mode == ['3'] then
        jugarSmart x juginici size
    else if mode == ['4'] then
        jugarDuel x juginici size
    else
        modeJoc x size 

-- Main on s'inicialitza el tauler per poder jugar
main :: IO ()
main = do
    putStrLn "Tria la mida del taulell"
    aux <- getLine
    let input = map atoi (words aux)
    let tauler = crearTaulell input 
    imprimirTaulell tauler 
    let newtauler = changeStatus tauler "J"
    if input == [] then do
        modeJoc newtauler [6,7]
    else 
        modeJoc newtauler input
    